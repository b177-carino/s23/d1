// select a database
use <database name>

// when creating a new data base via the command line, the use command can be entered with a new of a database that doesn't yet exist. Once a new record is inserted into that database, the database will be created. 

// database = filing cabinet
// collection = drawer
// document/record = folder inside a drawer
// sub-documents (optional) = other files
// fields = file/folder content

/*
e.g doucment with no sub-documents:
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor"
	}
e.g. document with sub-documents
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor",
		address: {
			street: "123 Makati Street",
			city: "Makati",
			country: "Philippines"
		}
	}
*/